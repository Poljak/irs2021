import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle, Circle, Polygon
import numpy as np


class Plot:

    def __init__(self, robot, estimated_robot, landmarks, particles, r_from=-3.0, r_to=3.0):
        plt.ion()
        #self.fig = plt.figure()
        self.fig, self.ax = plt.subplots(1)
        #self.ax = self.fig.add_subplot(111)
        self.ax.set_xlim(r_from, r_to)
        self.ax.set_ylim(r_from, r_to)
        self.ax.set_aspect("equal")
        self.ax.set_xticks(np.arange(r_from, r_to, 0.5))
        self.ax.set_yticks(np.arange(r_from, r_to, 0.5))
        self.ax.set_axisbelow(True)
        plt.grid()
        self.ax.set_xlabel('X')
        self.ax.set_ylabel('Y')

        self.robot = robot
        self.chassis_patch = self.ax.add_patch(Polygon(self.robot.chassis[:, :2], closed=True, facecolor="#009688"))
        self.wheel_l_patch = self.ax.add_patch(Polygon(self.robot.wheel_l[:, :2], closed=True, facecolor="#212121"))
        self.wheel_r_patch = self.ax.add_patch(Polygon(self.robot.wheel_r[:, :2], closed=True, facecolor="#212121"))

        self.estimated_robot = estimated_robot
        self.e_chassis_patch = self.ax.add_patch(Polygon(self.estimated_robot.chassis[:, :2], closed=True, facecolor="#00ff0088"))
        self.e_wheel_l_patch = self.ax.add_patch(Polygon(self.estimated_robot.wheel_l[:, :2], closed=True, facecolor="#212121"))
        self.e_wheel_r_patch = self.ax.add_patch(Polygon(self.estimated_robot.wheel_r[:, :2], closed=True, facecolor="#212121"))

        for landmark in landmarks:
            self.ax.add_patch(
                Circle(xy=landmark, radius=0.1, facecolor="#f4511e88")
            )

        self.particles_scatter = self.ax.scatter(particles[:, 0], particles[:, 1], s=0.5, c="#ff1744")



    def update(self):
        self.chassis_patch.xy = self.robot.chassis[:, :2]
        self.wheel_l_patch.xy = self.robot.wheel_l[:, :2]
        self.wheel_r_patch.xy = self.robot.wheel_r[:, :2]

        self.e_chassis_patch.xy = self.estimated_robot.chassis[:, :2]
        self.e_wheel_l_patch.xy = self.estimated_robot.wheel_l[:, :2]
        self.e_wheel_r_patch.xy = self.estimated_robot.wheel_r[:, :2]

        self.fig.canvas.draw_idle()