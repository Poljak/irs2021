from plot import Plot, plt
from robot import Robot, np, l, phi_max 

class Goal:
    def __init__(self, i_g=np.zeros(3, dtype=float)):
        self.i_g = i_g

from pid import PID

class GoToGoalController:
    def __init__(self, robot, goal):
        self.goal = goal
        self.robot = robot
        self.pid = PID()
        
    def __call__(self, dt):
        e = np.arctan2(self.goal.i_g[1] - self.robot.i_xi[1], self.goal.i_g[0] - self.robot.i_xi[0]) - self.robot.i_xi[2]
        e = np.arctan2(np.sin(e), np.cos(e))

        r_omega = self.pid(e, dt)
        r_v_dot = 1

        r_v_dot_feas, r_omega_feas  = self.robot.feas(r_v_dot, r_omega,dt)

        return r_v_dot_feas, r_omega_feas

class StopController:
    def __call__(self, dt):
        return 0, 0

if __name__ == "__main__":
    robot = Robot([-1.7,-1.7,0])
    goal = Goal(np.array([1, 0.5, 0], dtype=float))
    
    #obstacles = []
    
    plot = Plot(robot, goal, [])
    
    dt = 0.01
    plt.draw()

    gtgc = GoToGoalController(robot, goal)
    sc = StopController()
    
    ac = gtgc

    t = 0
    while True:
        t += dt

        if np.linalg.norm(robot.i_xi[:2] - goal.i_g[:2]) < l:
            ac = sc
        else:
            ac = gtgc

        r_v_dot, r_omega = ac(dt)
        phi_dot_l, phi_dot_r = robot.r_inverse_kinematics(r_v_dot, r_omega)
        i_xi_dot = robot.forward_kinematics(phi_dot_l, phi_dot_r)
        robot.update_state(i_xi_dot, dt)
        plot.update()
        plt.pause(dt)

    plt.waitforbuttonpress()
