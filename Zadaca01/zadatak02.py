#%%%
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from scipy.linalg import expm, sinm, cosm
#%%
def skew(omega):
    M = np.array([
            [0, -omega[2],omega[1]],
            [omega[2],0,-omega[0]],
            [-omega[1], omega[0], 0]
            ])

    return M

if __name__ == "__main__":
    plt.ion()
    fig = plt.figure()
    ax = fig.add_subplot(111, projection="3d")
    
    ax.set_xlim(-5,5)
    ax.set_ylim(-5,5)
    ax.set_zlim(-5,5)
    
    ax.set_xlabel("x")
    ax.set_ylabel("y")
    ax.set_zlabel("z")
    
    points_initial = np.array([
            [1,0,0],
            [0,1,0],
            [0,0,1],
            [0,0,0]
            ], dtype = float )
    
    points_initial2 = np.array([
            [1,0,0],
            [0,1,0],
            [0,0,1],
            [0,0,0]
            ], dtype = float )  
    
    points = np.zeros(points_initial.shape, dtype=float)
    points2 = np.zeros(points_initial2.shape, dtype=float)
    
    
    sc = ax.scatter(points_initial[:, 0], points_initial[:, 1], points_initial[:, 2], color="red")
    sc2 = ax.scatter(points_initial2[:, 0], points_initial2[:, 1], points_initial2[:, 2], color="blue")
    
    
# =============================================================================
#  Prvi dio zadatka 
# =============================================================================
    
    # Promjena zadatka 1 ili 2 
    zadatak = 2
    if(zadatak == 1):
        #Inicijalizacija (ishodiste)
        j_p_1 = np.array([0, 0, 0], dtype=float)
        j_p_2 = np.array([0, 0, 0], dtype=float)
        
        #Kutne brzina
        omega1 = np.array([0, 0, 0], dtype = float)
        omega2 = np.array([np.pi, -np.pi, 0], dtype = float)
        
        #Translacijske brzine
        j_p_dot1 = np.array([1, 1, 0], dtype=float)
        j_p_dot2 = np.array([2, 0, 2], dtype=float)
        
        
        #Vrijeme uzrokovanja
        dt = 0.01
        
        skew_r1 = expm(skew(omega1*dt))
        skew_r2 = expm(skew(omega2*dt)) 
        
        j_R_1 = np.eye(3, dtype=float)
        j_R_2 = np.eye(3, dtype=float)
        
        j=0
        while True:
            j += 1
            
            j_p_1 +=  j_p_dot1 * dt
            j_p_2 +=  j_p_dot2 * dt
            
            j_R_1 = j_R_1 @ skew_r1
            j_R_2 = j_R_2 @ skew_r2
            
            for i in range(points_initial.shape[0]):
                points[i] = j_R_1 @ points_initial[i] + j_p_1
                points2[i] = j_R_1 @ (j_R_2 @ points_initial2[i] + j_p_2) + j_p_1
            
            sc._offsets3d = points[: ,0], points[: ,1], points[: ,2]
            sc2._offsets3d = points2[: ,0], points2[: ,1], points2[: ,2]
            fig.canvas.draw_idle()
            
            if j==100:
                break
            plt.pause(dt)

    else:
# =============================================================================
# Drugi dio zadatka
# =============================================================================
        #Inicijalizacija (ishodiste)
        j_p_1 = np.array([0, 0, 0], dtype=float)
        j_p_2 = np.array([0, 0, 0], dtype=float)
        
        #Kutne brzina
        omega1 = np.array([2*np.pi, 0, 0], dtype = float)
        omega2 = np.array([0, 8*np.pi, 0], dtype = float)
        
        #Translacijske brzine
        j_p_dot1 = np.array([1, 1, 0], dtype=float)
        j_p_dot2 = np.array([-2, -3, 0], dtype=float)
        
        
        #Vrijeme uzrokovanja
        dt = 0.01
        
        skew_r1 = expm(skew(omega1*dt))
        skew_r2 = expm(skew(omega2*dt)) 
        
        j_R_1 = np.eye(3, dtype=float)
        j_R_2 = np.eye(3, dtype=float)
        
        j=0
        while True:
            j += 1
            
            j_p_1 +=  j_p_dot1 * dt
            j_p_2 +=  j_p_dot2 * dt
            
            j_R_1 = j_R_1 @ skew_r1
            j_R_2 = j_R_2 @ skew_r2
            
            for i in range(points_initial.shape[0]):
                points[i] = j_R_1 @ points_initial[i] + j_p_1
                points2[i] = j_R_1 @ (j_R_2 @ points_initial2[i] + j_p_2) + j_p_1
            
            sc._offsets3d = points[: ,0], points[: ,1], points[: ,2]
            sc2._offsets3d = points2[: ,0], points2[: ,1], points2[: ,2]
            fig.canvas.draw_idle()
            
            if j==100:
                break
            plt.pause(dt)
    
    while True:
        if plt.waitforbuttonpress():
            break