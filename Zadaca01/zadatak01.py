#%%%
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from scipy.linalg import expm, sinm, cosm

#%%
def skew(omega):
    M = np.array([
            [0, -omega[2],omega[1]],
            [omega[2],0,-omega[0]],
            [-omega[1], omega[0], 0]
            ])

    return M

if __name__ == "__main__":
    plt.ion()
    fig = plt.figure()
    ax = fig.add_subplot(111, projection="3d")
    
    ax.set_xlim(-5,5)
    ax.set_ylim(-5,5)
    ax.set_zlim(-5,5)
    
    ax.set_xlabel("x")
    ax.set_ylabel("y")
    ax.set_zlabel("z")
    
    points_initial = np.array([
            [1,0,0],
            [0,1,0],
            [0,0,1],
            [0,0,0]
            ], dtype = float )    
    sc = ax.scatter(points_initial[:, 0], points_initial[:, 1], points_initial[:, 2])

    dt = 0.01
    points = points_initial

    #Kutne brzina
    omega1 = np.array([0, 0.25*np.pi, 0], dtype = float)
    omega2 = np.array([np.pi, 0, 0], dtype = float)
    
    i=0
    while True:
        i+=1
        if i<100:
            omega = omega1
        else:
            omega = omega2
        R = expm(skew(omega*dt))
        for j in range(points_initial.shape[0]):
            points[j] = R @ points_initial[j]
        
        sc._offsets3d = points[: ,0], points[: ,1], points[: ,2]
        fig.canvas.draw_idle()
        if i==200:
            break
        plt.pause(dt)
    
    #tehnicka stvar
    while True:
        if plt.waitforbuttonpress():
            break