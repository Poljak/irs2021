import numpy as np
import matplotlib.pyplot as plt
from matplotlib.patches import Polygon

def init_plot(r_from=-1, r_to=1):
    plt.ion()
    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.set_xlim(r_from, r_to)
    ax.set_ylim(r_from, r_to)
    ax.set_aspect("equal")
    # mreža (ax.set_xticks, ...)
    plt.grid()
    ax.set_xlabel("x")
    ax.set_ylabel("y")
    return fig, ax


def R(theta):
    return np.array([
        [np.cos(theta), np.sin(theta), 0],
        [-np.sin(theta), np.cos(theta), 0],
        [0, 0, 1]
    ], dtype=float)

if __name__ == "__main__":
    fig, ax = init_plot(-1,2)
    plt.draw()
    
    # inicijalno stanje robota
    i_xi = np.array([0, 0, 0], dtype=float)
    # stanje robvota u gibajućem koordinatnom sustavu uvijek [0, 0, 0]
    l = 0.32
    r = 0.05
    
    # duljina šasije
    d=0.1
    # širina kotača
    wheel_w = 0.03

    a_1 = np.pi / 5
    a_2 = -np.pi / 5
    a_3 = np.pi
    b_1 = -a_1
    b_2 = -a_2 + 13/10 * np.pi
    b_3 = 0.0
    

    wheel_points = [
        np.array([-wheel_w / 2, -wheel_w / 2, 0], dtype=float),
        np.array([-wheel_w / 4, -wheel_w / 2, 0], dtype=float),
        np.array([-wheel_w / 4, -wheel_w / 2-0.04, 0], dtype=float),
        np.array([-wheel_w / 2, -wheel_w / 2-0.04, 0], dtype=float),
        np.array([-wheel_w / 2, -wheel_w / 2-0.15, 0], dtype=float),
        np.array([wheel_w / 2, -wheel_w / 2-0.15, 0], dtype=float),
        np.array([wheel_w / 2, -wheel_w / 2-0.04, 0], dtype=float),
        np.array([wheel_w / 4, -wheel_w / 2-0.04, 0], dtype=float),
        np.array([wheel_w / 4, -wheel_w / 2, 0], dtype=float),
        np.array([wheel_w / 2, -wheel_w / 2, 0], dtype=float),
        np.array([wheel_w / 2, wheel_w / 2, 0], dtype=float),
        np.array([-wheel_w / 2, wheel_w / 2, 0], dtype=float)
    ]

    chassis_points = [
            np.array([a_1*l+0.05, a_1*l, 0]),
            np.array([a_1*l+0.05, -a_1*l, 0]),
            np.array([-l, -a_1*l, 0]),
            np.array([-l, a_1*l, 0])
    ]

    wheel_1 = np.array([
        R(i_xi[2]).transpose() @ R(a_1).transpose() @ (R(b_1).transpose() @ point + np.array([l, 0, 0])) + np.array([i_xi[0], i_xi[1], 0]) for point in wheel_points
    ])

    wheel_2 = np.array([
        R(i_xi[2]).transpose() @ R(a_2).transpose() @ (R(b_2).transpose() @ point + np.array([l, 0, 0])) + np.array([i_xi[0], i_xi[1], 0]) for point in wheel_points
    ])
    
    wheel_3 = np.array([
        R(i_xi[2]).transpose() @ R(a_3).transpose() @ (R(b_3).transpose() @ point + np.array([l, 0, 0])) + np.array([i_xi[0], i_xi[1], 0]) for point in wheel_points
    ])

    chassis = np.array([
        R(i_xi[2]).transpose() @ point + np.array([i_xi[0], i_xi[1], 0]) for point in chassis_points
    ])

    wheel_1_patch = ax.add_patch(Polygon(wheel_1[:, :2], facecolor="#000000"))
    wheel_2_patch = ax.add_patch(Polygon(wheel_2[:, :2], facecolor="#000000"))
    wheel_3_patch = ax.add_patch(Polygon(wheel_3[:, :2], facecolor="#000000"))
    chassis_patch = ax.add_patch(Polygon(chassis[:, :2], facecolor="#64ccc3", alpha=0.7))

    dt = 0.01

    v_dots = np.array([1,3,1,2,1,5])
    omegas = np.array([2*np.pi, 0, -2*np.pi, -1.5*np.pi, 2*np.pi, 0])
    move = np.array([25,35,55,60,50,25]) 
    
    for k in range(move.shape[0]):
        v_dot = v_dots[k]
        omega = omegas[k]
        i = 0
        
        while True:
            M = np.array([
                [np.sin(a_1 + b_1),     -np.cos(a_1 + b_1),     -l*np.cos(b_1)],
                [np.sin(a_2 + b_2),     -np.cos(a_2 + b_2),     -l*np.cos(b_2)],
                [np.sin(a_3 + b_3),     -np.cos(a_3 + b_3),     -l*np.cos(b_3)],
                [np.cos(a_1 + b_1),     np.sin(a_1 + b_1),      d+l*np.sin(b_1)],
                [np.cos(a_2 + b_2),     np.sin(a_2 + b_2),      d+l*np.sin(b_2)],
                [np.cos(a_3 + b_3),     np.sin(a_3 + b_3),      d+l*np.sin(b_3)]
            ])
            
            phi_dots = M \
                       @ R(i_xi[2]) \
                       @ np.array([v_dot * np.cos(i_xi[2]), v_dot * np.sin(i_xi[2]), omega], dtype=float)
            
            #Proklizavanje
            beta_1_dot = phi_dots[3]/(-d)
            beta_2_dot = phi_dots[4]/(-d)
            beta_3_dot = phi_dots[5]/(-d)
            
            i_xi_dot = R(i_xi[2]).transpose()\
                       @ np.linalg.inv( M.transpose() @ M )\
                       @ M.transpose()\
                       @ phi_dots
                       
            i_xi += i_xi_dot * dt
            
            #kn
            b_1 += beta_1_dot * dt
            b_2 += beta_2_dot * dt
            b_3 += beta_3_dot * dt
    
            wheel_1 = np.array([
                R(i_xi[2]).transpose() @ R(a_1).transpose() @ (
                            R(b_1).transpose() @ point + np.array([l, 0, 0])) + np.array([i_xi[0], i_xi[1], 0]) for point
                in wheel_points
            ])
    
            wheel_2 = np.array([
                R(i_xi[2]).transpose() @ R(a_2).transpose() @ (
                            R(b_2).transpose() @ point + np.array([l, 0, 0])) + np.array([i_xi[0], i_xi[1], 0]) for point
                in wheel_points
            ])
            
            wheel_3 = np.array([
                R(i_xi[2]).transpose() @ R(a_3).transpose() @ (
                            R(b_3).transpose() @ point + np.array([l, 0, 0])) + np.array([i_xi[0], i_xi[1], 0]) for point
                in wheel_points
            ])
    
            chassis = np.array([
                R(i_xi[2]).transpose() @ point + np.array([i_xi[0], i_xi[1], 0]) for point in chassis_points
            ])
        
            wheel_1_patch.xy = wheel_1[:, :2]
            wheel_2_patch.xy = wheel_2[:, :2]
            wheel_3_patch.xy = wheel_3[:, :2]
            chassis_patch.xy = chassis[:, :2]
    
            fig.canvas.draw_idle()
            plt.pause(dt)
    
            #step 25sec, 30sec,...
            i += 1
            if i == move[k]:
                break
        